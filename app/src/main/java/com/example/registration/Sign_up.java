package com.example.registration;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import java.net.PasswordAuthentication;
import java.security.KeyStore;

public class Sign_up extends AppCompatActivity {
    DatabaseHelper db;
    EditText mTextUsername;
    EditText mTextPassword;
    EditText mTextCnfPassword;
    EditText mTextEmail;
    EditText mTextPhone;
    Button mButtonJOIN_US;
    TextView mTextViewRegister;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        db = new DatabaseHelper(this);
         mTextUsername = (EditText)findViewById(R.id.edittext_username);
       mTextPassword = (EditText)findViewById(R.id.edittext_password);
        mTextCnfPassword = (EditText)findViewById(R.id.edittext_cnf_password);
        mTextEmail = (EditText)findViewById(R.id.edittext_Email);
        mTextPhone = (EditText)findViewById(R.id.edittext_Phone);
        mButtonJOIN_US = (Button)findViewById(R.id.button_JOIN_US);
        Button mButtonRegister =(Button)findViewById(R.id.join);
        mButtonRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = mTextUsername.getText().toString().trim();
                String pwd = mTextPassword.getText().toString().trim();
                String cnf_pwd = mTextCnfPassword.getText().toString().trim();
//                String email = mTextEmail.getText().toString().trim();
             //   String phone = mTextPhone.getText().toString().trim();

               if(pwd.equals(cnf_pwd)){
                    long val = db.addUser(user,pwd);
                   String check = val+"";
                   Log.e("check" ,check );
                    if(val > 0){
                        Toast.makeText(Sign_up.this,"You have registered",Toast.LENGTH_SHORT).show();
                        Intent moveToLogin = new Intent(Sign_up.this,Log_in.class);
                        startActivity(moveToLogin);
                    }
                   else{
                        Toast.makeText(Sign_up.this,"Registeration Error",Toast.LENGTH_SHORT).show();
                   }
             }
               else{
                    Toast.makeText(Sign_up.this,"Password is not matching",Toast.LENGTH_SHORT).show();
                }
           }
        });


    }

}